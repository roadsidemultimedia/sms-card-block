<?php
/*
  Plugin Name: sms card block
  Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-card-block
  Section: Card Block
  Author: PageLines
  Author URI: http://www.pagelines.com
  Description: A card block
  Class Name: sms_card_block
  Edition: pro
  Filter: component
  Loading: active
  Version: 1.0.0
  PageLines: true
  Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-card-block
  Bitbucket Branch: master
*/

if( ! class_exists( 'PageLinesSectionFactory' ) )
  return;

class sms_card_block extends PageLinesSection {

  function section_opts(){
    $options = array();

    $options[] = array(
      'title' => __( 'Image', 'pagelines' ),
      'type'  => 'multi',
      'key' => 'card_block_content',
      'col' => 2,
      'opts'  => array(
        array(
          'type'  => 'image_upload',
          'key' => 'card_block_image_url',
          'label' => __( 'Image', 'pagelines' ),
          'has_alt' => true,
        )
      )

    );

    $options[] = array(

      'title' => __( 'Copywriting', 'pagelines' ),
      'type'  => 'multi',
      'opts'  => array(
        array(
          'label'         => __( 'Title', 'pagelines' ),
          'key'           => 'card_block_title',
          'type'          => 'text',
          'default'       => 'Card Title',
        ),
        array(
          'type'          => 'textarea',
          'key'           => 'card_block_content',
          'label'         => __( 'Content', 'pagelines' ),
        ),
      )
    );
    $options[] = array(

      'title' => __( 'Button', 'pagelines' ),
      'type'  => 'multi',
      'col' => 2,
      'opts'  => array(
        array(
          'label'         => __( 'Display button?', 'pagelines' ),
          'key'           => 'card_block_button_enabled',
          'type'          => 'check',
        ),
        array(
          'label'         => __( 'Button Text', 'pagelines' ),
          // 'ref'         => __( 'This option uses CSS padding shorthand. For example, use "15px 30px" for 15px padding top/bottom, and 30 left/right.', 'pagelines' ),
          'key'           => 'card_block_button_text',
          'type'          => 'text',
          'default'       => 'Card Title',
        ),
        array(
          'label'         => __( 'Button URL', 'pagelines' ),
          // 'ref'         => __( 'This option uses CSS padding shorthand. For example, use "15px 30px" for 15px padding top/bottom, and 30 left/right.', 'pagelines' ),
          'key'           => 'card_block_button_url',
          'type'          => 'text',
          'default'       => 'Learn More',
        ),
      )

    );

    return $options;
  }
  function section_template(){
    $title = ($this->opt('card_block_title')) ? $this->opt('card_block_title') : 'Card Title';
    $content = ($this->opt('card_block_content')) ? $this->opt('card_block_content') : 'no content';
    $button = ($this->opt('card_block_button_text')) ? $this->opt('card_block_button_text') : 'no button';
    $button_url = ($this->opt('card_block_button_url')) ? $this->opt('card_block_button_url') : '';
    $button_enabled = ($this->opt('card_block_button_enabled')) ? $this->opt('card_block_button_enabled') : false;
    $image_url = ($this->opt('card_block_image_url')) ? $this->opt('card_block_image_url') : '';

    ?>
    <div class="card-block">
      <div class="front">
        <img src="<?php echo $image_url ?>" />
        <div class="card-block--heading"><?php echo $title ?></div>
      </div>
      <div class="back">
         <div class="card-block--heading"><?php echo $title ?></div>
        <div class="card-block--body">
          <div class="clear margin-bottom"><?php echo $content ?></div>
          <?php if( $button_enabled ) : ?>
            <a href='<?php echo $button_url ?>' class='btn btn-primary'><?php echo $button ?></a>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <?php

  }

}
